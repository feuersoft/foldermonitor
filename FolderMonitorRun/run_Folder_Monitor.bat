@echo off

set JAVA=%JAVA_HOME%\bin\javaw

set FM_CLASSPATH=lib\FolderMonitor.jar;lib\sun\javamail\lib\mailapi.jar;lib\sun\javamail\lib\smtp.jar;lib\sun\javamail\lib\dsn.jar;lib\sun\javamail\lib\imap.jar;lib\sun\javamail\lib\pop3.jar;lib\netbeans\appframework-1.0.3.jar;lib\netbeans\swing-worker-1.1.jar

set VM_OPTIONS=-Xms128m -Xmx1024m

set MAIN_CLASS=foldermonitor.FolderMonitorMainJFrame

start "Folder Monitor" "%JAVA%" -classpath "%FM_CLASSPATH%" %VM_OPTIONS% %MAIN_CLASS%

