/*
 * Copyright (C) 2020, FeuerSoft.
 */
package foldermonitor;

import java.awt.Image;
import java.io.File;
import java.text.SimpleDateFormat;
import javax.swing.ImageIcon;

/**
 * The <code>Utils</code> class provides common services.
 * All methods are static and should not store state between
 * calls.
 *
 * @author Fritz Feuerbacher
 * @since JDK1.6
 */
public class Utils
{
  private static final String VERSION = "1.1.0";

   public  static final String APP_TITLE = "Folder Monitor";
   private static final String VER_TITLE = " Version: ";
   private static final String NL = "\r\n";
   private static final String USER_MAN = "User Manual is located in the Folder Monitor help directory.";
   public  static final String COPYRIGHT = "Copyright (C) 2020, FeuerSoft";
   public  static final String AUTHOR = "Author: Fritz Feuerbacher";
   public  static final String JRE_VERSION = System.getProperty("java.version");
   private static final String ABOUT_BOX = APP_TITLE + VER_TITLE + VERSION + NL +
                                    USER_MAN + NL + AUTHOR + NL + COPYRIGHT + NL + NL
                                    + "Java runtime version: " + JRE_VERSION;

   public static final long MILLISECS_IN_SEC  = 1000L;
   public static final long MILLISECS_IN_MIN  = 60000L;
   public static final long MILLISECS_IN_HOUR = 3600000L;
   public static final long MILLISECS_IN_DAY  = 86400000L;
   public static final long MILLISECS_IN_3DAY = 259200000L;
   public static final long MILLISECS_IN_WEEK = 604800000L;

   public static final String COMMENT_DELIM = ":";
   public static final String INPUT_ASK = "Select an Option";

   public static final String APP_CFG_FILE_NAME = "monitor_config.dat";
   public static final String MAIL_CFG_FILE_NAME = "mail_config.cfg";

   public static final SimpleDateFormat dtgDateFormat =
                                new SimpleDateFormat("ddkkmm'Z'MMMyyyy");

   // The File Monitor icon used for the upper left window panes.
   private static ImageIcon imgIconFolderMonitor = null;

   public static String getExtension(File f)
   {
     String ext = null;
     if (null != f)
     {
       String s = f.getName();
       int i = s.lastIndexOf('.');

       if (i>0 && i<s.length()-1)
       {
         ext = s.substring(i+1).toLowerCase();
       }
     }
     return ext; 
   }

   public static String getABOUT_BOX()
   {
      return ABOUT_BOX;
   }

   public static String getApp_Title()
   {
      return APP_TITLE;
   }

   /* Returns an ImageIcon, or null if the path was invalid. */
   public static Image getFolderMonitorImage()
   {
      ImageIcon icon = Utils.getFolderMonitorIcon();
        
      if (null != icon)
      {
          return icon.getImage();
      }
      else
      {
          return null;
      }
   }

   /* Returns an ImageIcon, or null if the path was invalid. */
   public static ImageIcon getFolderMonitorIcon()
   {
     if (null == imgIconFolderMonitor)
     {
       try
       {
         ClassLoader cld = Thread.currentThread().getContextClassLoader();
         java.net.URL imgURL = cld.getResource("about.png");

         if (imgURL != null)
         {
           imgIconFolderMonitor = new ImageIcon(imgURL);
         }
       }
       catch (Exception e)
       {
          System.err.println("Cannot get icon! " + e.getMessage());
       }
     }

     return imgIconFolderMonitor;
   }
}
