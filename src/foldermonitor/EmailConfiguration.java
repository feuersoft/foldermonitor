/*
 * Copyright (C) 2020, FeuerSoft.
 */
package foldermonitor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.mail.Session;

/**
 * @author Fritz Feuerbacher
 */
public class EmailConfiguration
  implements Serializable
{
   private static final long serialVersionUID = 02L;

   public boolean serverRequiresAuth = false;
   public List<String> monitorFolders = new ArrayList<>();
   public List<String> copyFolders = new ArrayList<>();
   public List<String> toAddresses = new ArrayList<>();
   public String loginPassword = "admin";
   public String loginName = "admin";
   public Integer serverPort = 25;
   public String serverHost = "localhost";
   public String fromAddress = "admin@192.168.0.17";
   public String subject = "My Subject";
   public Boolean combineReports = false;
   public boolean useSubject = false;
   public boolean sendEmail = false;
   public boolean copyFiles = false;
   public Integer timerDelay = 5;
   public Integer copyPerDelay = 100;
   transient public Session mailSession = null;
}
